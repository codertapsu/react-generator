# ReactJs Generator CLI

ReactJs Generator CLI will help you create files in a ReactJs Project quickly.

## Installation

```bash
git clone https://gitlab.com/codertapsu/react-generator.git
```

Open project then following steps:

```bash
npm install
npm link
```

## Usage

```bash
rg <type> <name> [option: <style format type>]
```

To generate Service:

```bash
rg service example
rg s example
```

To generate Component:

```bash
rg component example scss
rg c example scss
```

To generate Module:

```bash
rg module example scss
rg m example scss
```

## Contributing

Pull requests are welcome.

## License

UNLICENSED
