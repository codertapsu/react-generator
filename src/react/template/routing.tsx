import React from 'react';
import { Switch, Route } from 'react-router-dom';

const Routes: IRouteModel[] = [];

const ExampleRoutes = (
  <Switch>
    {Routes.map((route, index) => (
      <Route key={index} path={route.path} component={route.component} />
    ))}
  </Switch>
);

export { ExampleRoutes };

/**
 * Move below to global folder
 */
interface IRouteModel {
  path: string;
  isExact: boolean;
  component: any;
  routeGuards: GuardFunction[];
}
type GuardFunction = () => void;