import React, { FC } from 'react';
import './componentName.component.css';

const ComponentName: FC = (): JSX.Element => {
  return (
    <div className={'componentName-container'}>
      <p>ComponentName work!</p>
    </div>
  );
};

export default ComponentName;
