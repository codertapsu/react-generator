import { existsSync, writeFile, mkdir, readFile } from 'fs-extra';

const upperCaseFirstLetter = (name: string) => name.charAt(0).toUpperCase() + name.slice(1);

const generate = async (typeToGen: string, name: string, styleType = 'css') => {
  try {
    const className = name.replace(/\-+[\S]/gmi, (value) => value.substr(-1).toUpperCase());
    const fileName = name.replace(/\-+[\S]/gmi, (value) => `-${value.substr(-1).toLowerCase()}`);
    switch (typeToGen) {
      case 'component':
      case 'c': {
        if (!existsSync(`./${fileName}`)) {
          await mkdir(`./${fileName}`);
        }
        const styleFile = `./${fileName}/${fileName}.component.${styleType}`;
        writeFile(styleFile, '', 'utf8');

        const cloneTsxFile = await readFile(__dirname + '/../template/component.tsx', 'utf8');
        const formatTsxFile = cloneTsxFile.replace(/ComponentName/gm, upperCaseFirstLetter(className)).replace(/componentName/gm, fileName).replace(/css/gm, styleType);
        const tsxFile = `./${fileName}/${fileName}.component.tsx`;
        writeFile(tsxFile, formatTsxFile, 'utf8');

        break;
      }
      case 'service':
      case 's': {
        const cloneServiceFile = await readFile(__dirname + '/../template/service.ts', 'utf8');
        const serviceFile = `./${fileName}.service.ts`;
        writeFile(serviceFile, cloneServiceFile.replace(/Template/gim, upperCaseFirstLetter(className)), 'utf8');
        break;
      }
      case 'module':
      case 'm': {
        if (!existsSync(`./${fileName}`)) {
          await mkdir(`./${fileName}`);
        }
        await mkdir(`./${fileName}/hooks`);
        await mkdir(`./${fileName}/services`);
        await mkdir(`./${fileName}/components`);
        await mkdir(`./${fileName}/models`);
        await mkdir(`./${fileName}/constants`);

        const styleFile = `./${fileName}/${fileName}.component.${styleType}`;
        writeFile(styleFile, '', 'utf8');

        const cloneTsxFile = await readFile(__dirname + '/../template/component.tsx', 'utf8');
        const formatTsxFile = cloneTsxFile.replace(/ComponentName/gm, upperCaseFirstLetter(className)).replace(/componentName/gm, fileName).replace(/css/gm, styleType);
        const tsxFile = `./${fileName}/${fileName}.component.tsx`;
        writeFile(tsxFile, formatTsxFile, 'utf8');

        const cloneServiceFile = await readFile(__dirname + '/../template/service.ts', 'utf8');
        const serviceFile = `./${fileName}/services/${fileName}.service.ts`;
        writeFile(serviceFile, cloneServiceFile.replace(/Template/gim, upperCaseFirstLetter(className)), 'utf8');

        const cloneRoutingFile = await readFile(__dirname + '/../template/routing.tsx', 'utf8');
        const formatRoutingFile = cloneRoutingFile.replace(/ExampleRoutes/gmi, `${upperCaseFirstLetter(className)}Routes`);
        const routingFile = `./${fileName}/${fileName}.routing.tsx`;
        writeFile(routingFile, formatRoutingFile, 'utf8');
        break;
      }
      default:

    }
  } catch (error) {
    return console.log(error);
  }
}

export { generate };
