import { generate } from './generator';

const listen = () => {
  const clientArgs = process.argv.slice(2);
  const [ type, name, style ] = clientArgs;
  generate(type, name, style);
}
export { listen };
