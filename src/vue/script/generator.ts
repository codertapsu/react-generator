import { existsSync, writeFile, mkdir, readFile } from 'fs-extra';

const upperCaseFirstLetter = (name: string) => name.charAt(0).toUpperCase() + name.slice(1);

const generate = async (typeToGen: string, name: string) => {
  try {
    const className = name.replace(/\-+[\S]/gim, value => value.substr(-1).toUpperCase());
    const fileName = name.replace(/\-+[\S]/gim, value => `-${value.substr(-1).toLowerCase()}`);
    switch (typeToGen) {
      case 'component':
      case 'c': {
        if (!existsSync(`./${fileName}`)) {
          await mkdir(`./${fileName}`);
        }
        const styleFile = `./${fileName}/${fileName}.scss`;
        await writeFile(styleFile, '', 'utf8');

        const cloneTsFile = await readFile(__dirname + '/../template/component-placeholder/component-placeholder.ts', 'utf8');
        const formatTsFile = cloneTsFile.replace(/ComponentPlaceHolder/gm, upperCaseFirstLetter(className));
        const tsFile = `./${fileName}/${fileName}.ts`;
        await writeFile(tsFile, formatTsFile, 'utf8');

        const cloneHtmlFile = await readFile(__dirname + '/../template/component-placeholder/component-placeholder.html', 'utf8');
        const formatHtmlFile = cloneHtmlFile.replace(/component-placeholder/gm, fileName);
        const htmlFile = `./${fileName}/${fileName}.html`;
        await writeFile(htmlFile, formatHtmlFile, 'utf8');

        const cloneVueFile = await readFile(__dirname + '/../template/component-placeholder/component-placeholder.vue', 'utf8');
        const formatVueFile = cloneVueFile.replace(/component-placeholder/gm, fileName);
        const vueFile = `./${fileName}/${fileName}.vue`;
        await writeFile(vueFile, formatVueFile, 'utf8');

        break;
      }
      default: {
      }
    }
  } catch (error) {
    return console.log(error);
  }
};

export { generate };
