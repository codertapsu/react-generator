import { generate } from './generator';

const listen = () => {
  const clientArgs = process.argv.slice(2);
  const [type, name] = clientArgs;
  generate(type, name);
};
export { listen };
