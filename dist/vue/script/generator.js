"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generate = void 0;
var fs_extra_1 = require("fs-extra");
var upperCaseFirstLetter = function (name) { return name.charAt(0).toUpperCase() + name.slice(1); };
var generate = function (typeToGen, name) { return __awaiter(void 0, void 0, void 0, function () {
    var className, fileName, _a, styleFile, cloneTsFile, formatTsFile, tsFile, cloneHtmlFile, formatHtmlFile, htmlFile, cloneVueFile, formatVueFile, vueFile, error_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 13, , 14]);
                className = name.replace(/\-+[\S]/gim, function (value) { return value.substr(-1).toUpperCase(); });
                fileName = name.replace(/\-+[\S]/gim, function (value) { return "-".concat(value.substr(-1).toLowerCase()); });
                _a = typeToGen;
                switch (_a) {
                    case 'component': return [3 /*break*/, 1];
                    case 'c': return [3 /*break*/, 1];
                }
                return [3 /*break*/, 11];
            case 1:
                if (!!(0, fs_extra_1.existsSync)("./".concat(fileName))) return [3 /*break*/, 3];
                return [4 /*yield*/, (0, fs_extra_1.mkdir)("./".concat(fileName))];
            case 2:
                _b.sent();
                _b.label = 3;
            case 3:
                styleFile = "./".concat(fileName, "/").concat(fileName, ".scss");
                return [4 /*yield*/, (0, fs_extra_1.writeFile)(styleFile, '', 'utf8')];
            case 4:
                _b.sent();
                return [4 /*yield*/, (0, fs_extra_1.readFile)(__dirname + '/../template/component-placeholder/component-placeholder.ts', 'utf8')];
            case 5:
                cloneTsFile = _b.sent();
                formatTsFile = cloneTsFile.replace(/ComponentPlaceHolder/gm, upperCaseFirstLetter(className));
                tsFile = "./".concat(fileName, "/").concat(fileName, ".ts");
                return [4 /*yield*/, (0, fs_extra_1.writeFile)(tsFile, formatTsFile, 'utf8')];
            case 6:
                _b.sent();
                return [4 /*yield*/, (0, fs_extra_1.readFile)(__dirname + '/../template/component-placeholder/component-placeholder.html', 'utf8')];
            case 7:
                cloneHtmlFile = _b.sent();
                formatHtmlFile = cloneHtmlFile.replace(/component-placeholder/gm, fileName);
                htmlFile = "./".concat(fileName, "/").concat(fileName, ".html");
                return [4 /*yield*/, (0, fs_extra_1.writeFile)(htmlFile, formatHtmlFile, 'utf8')];
            case 8:
                _b.sent();
                return [4 /*yield*/, (0, fs_extra_1.readFile)(__dirname + '/../template/component-placeholder/component-placeholder.vue', 'utf8')];
            case 9:
                cloneVueFile = _b.sent();
                formatVueFile = cloneVueFile.replace(/component-placeholder/gm, fileName);
                vueFile = "./".concat(fileName, "/").concat(fileName, ".vue");
                return [4 /*yield*/, (0, fs_extra_1.writeFile)(vueFile, formatVueFile, 'utf8')];
            case 10:
                _b.sent();
                return [3 /*break*/, 12];
            case 11:
                {
                }
                _b.label = 12;
            case 12: return [3 /*break*/, 14];
            case 13:
                error_1 = _b.sent();
                return [2 /*return*/, console.log(error_1)];
            case 14: return [2 /*return*/];
        }
    });
}); };
exports.generate = generate;
