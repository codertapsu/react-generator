"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.listen = void 0;
var generator_1 = require("./generator");
var listen = function () {
    var clientArgs = process.argv.slice(2);
    var type = clientArgs[0], name = clientArgs[1];
    (0, generator_1.generate)(type, name);
};
exports.listen = listen;
