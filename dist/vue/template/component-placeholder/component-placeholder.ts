import { Options, Vue } from 'vue-class-component';

@Options({})
export default class ComponentPlaceHolder extends Vue {}
