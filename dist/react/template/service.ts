class TemplateService {
  private static instance: TemplateService;
  static getInstance(): TemplateService {
    if (!TemplateService.instance) {
      TemplateService.instance = new TemplateService();
    }
    return TemplateService.instance;
  }
  /**
   * The Singleton's constructor should always be private
   * to prevent direct construction calls with the `new` operator.
   */
  private constructor() { }
}

export { TemplateService };
