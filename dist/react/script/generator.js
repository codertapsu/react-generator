"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generate = void 0;
var fs_extra_1 = require("fs-extra");
var upperCaseFirstLetter = function (name) { return name.charAt(0).toUpperCase() + name.slice(1); };
var generate = function (typeToGen, name, styleType) {
    if (styleType === void 0) { styleType = 'css'; }
    return __awaiter(void 0, void 0, void 0, function () {
        var className, fileName, _a, styleFile, cloneTsxFile, formatTsxFile, tsxFile, cloneServiceFile, serviceFile, styleFile, cloneTsxFile, formatTsxFile, tsxFile, cloneServiceFile, serviceFile, cloneRoutingFile, formatRoutingFile, routingFile, error_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 19, , 20]);
                    className = name.replace(/\-+[\S]/gmi, function (value) { return value.substr(-1).toUpperCase(); });
                    fileName = name.replace(/\-+[\S]/gmi, function (value) { return "-".concat(value.substr(-1).toLowerCase()); });
                    _a = typeToGen;
                    switch (_a) {
                        case 'component': return [3 /*break*/, 1];
                        case 'c': return [3 /*break*/, 1];
                        case 'service': return [3 /*break*/, 5];
                        case 's': return [3 /*break*/, 5];
                        case 'module': return [3 /*break*/, 7];
                        case 'm': return [3 /*break*/, 7];
                    }
                    return [3 /*break*/, 18];
                case 1:
                    if (!!(0, fs_extra_1.existsSync)("./".concat(fileName))) return [3 /*break*/, 3];
                    return [4 /*yield*/, (0, fs_extra_1.mkdir)("./".concat(fileName))];
                case 2:
                    _b.sent();
                    _b.label = 3;
                case 3:
                    styleFile = "./".concat(fileName, "/").concat(fileName, ".component.").concat(styleType);
                    (0, fs_extra_1.writeFile)(styleFile, '', 'utf8');
                    return [4 /*yield*/, (0, fs_extra_1.readFile)(__dirname + '/../template/component.tsx', 'utf8')];
                case 4:
                    cloneTsxFile = _b.sent();
                    formatTsxFile = cloneTsxFile.replace(/ComponentName/gm, upperCaseFirstLetter(className)).replace(/componentName/gm, fileName).replace(/css/gm, styleType);
                    tsxFile = "./".concat(fileName, "/").concat(fileName, ".component.tsx");
                    (0, fs_extra_1.writeFile)(tsxFile, formatTsxFile, 'utf8');
                    return [3 /*break*/, 18];
                case 5: return [4 /*yield*/, (0, fs_extra_1.readFile)(__dirname + '/../template/service.ts', 'utf8')];
                case 6:
                    cloneServiceFile = _b.sent();
                    serviceFile = "./".concat(fileName, ".service.ts");
                    (0, fs_extra_1.writeFile)(serviceFile, cloneServiceFile.replace(/Template/gim, upperCaseFirstLetter(className)), 'utf8');
                    return [3 /*break*/, 18];
                case 7:
                    if (!!(0, fs_extra_1.existsSync)("./".concat(fileName))) return [3 /*break*/, 9];
                    return [4 /*yield*/, (0, fs_extra_1.mkdir)("./".concat(fileName))];
                case 8:
                    _b.sent();
                    _b.label = 9;
                case 9: return [4 /*yield*/, (0, fs_extra_1.mkdir)("./".concat(fileName, "/hooks"))];
                case 10:
                    _b.sent();
                    return [4 /*yield*/, (0, fs_extra_1.mkdir)("./".concat(fileName, "/services"))];
                case 11:
                    _b.sent();
                    return [4 /*yield*/, (0, fs_extra_1.mkdir)("./".concat(fileName, "/components"))];
                case 12:
                    _b.sent();
                    return [4 /*yield*/, (0, fs_extra_1.mkdir)("./".concat(fileName, "/models"))];
                case 13:
                    _b.sent();
                    return [4 /*yield*/, (0, fs_extra_1.mkdir)("./".concat(fileName, "/constants"))];
                case 14:
                    _b.sent();
                    styleFile = "./".concat(fileName, "/").concat(fileName, ".component.").concat(styleType);
                    (0, fs_extra_1.writeFile)(styleFile, '', 'utf8');
                    return [4 /*yield*/, (0, fs_extra_1.readFile)(__dirname + '/../template/component.tsx', 'utf8')];
                case 15:
                    cloneTsxFile = _b.sent();
                    formatTsxFile = cloneTsxFile.replace(/ComponentName/gm, upperCaseFirstLetter(className)).replace(/componentName/gm, fileName).replace(/css/gm, styleType);
                    tsxFile = "./".concat(fileName, "/").concat(fileName, ".component.tsx");
                    (0, fs_extra_1.writeFile)(tsxFile, formatTsxFile, 'utf8');
                    return [4 /*yield*/, (0, fs_extra_1.readFile)(__dirname + '/../template/service.ts', 'utf8')];
                case 16:
                    cloneServiceFile = _b.sent();
                    serviceFile = "./".concat(fileName, "/services/").concat(fileName, ".service.ts");
                    (0, fs_extra_1.writeFile)(serviceFile, cloneServiceFile.replace(/Template/gim, upperCaseFirstLetter(className)), 'utf8');
                    return [4 /*yield*/, (0, fs_extra_1.readFile)(__dirname + '/../template/routing.tsx', 'utf8')];
                case 17:
                    cloneRoutingFile = _b.sent();
                    formatRoutingFile = cloneRoutingFile.replace(/ExampleRoutes/gmi, "".concat(upperCaseFirstLetter(className), "Routes"));
                    routingFile = "./".concat(fileName, "/").concat(fileName, ".routing.tsx");
                    (0, fs_extra_1.writeFile)(routingFile, formatRoutingFile, 'utf8');
                    return [3 /*break*/, 18];
                case 18: return [3 /*break*/, 20];
                case 19:
                    error_1 = _b.sent();
                    return [2 /*return*/, console.log(error_1)];
                case 20: return [2 /*return*/];
            }
        });
    });
};
exports.generate = generate;
