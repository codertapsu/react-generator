const fse = require('fs-extra');
const path = require('path');

Promise.all(['vue', 'react'].map(name => fse.copy(path.join(__dirname, 'src', name, 'template'), path.join(__dirname, 'dist', name, 'template'), { overwrite: true })))
  .then(() => {
    console.log('success!');
  })
  .catch(err => {
    console.error(err);
  });
